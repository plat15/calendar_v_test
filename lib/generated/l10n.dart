// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Add New Event`
  String get add_new_event {
    return Intl.message(
      'Add New Event',
      name: 'add_new_event',
      desc: '',
      args: [],
    );
  }

  /// `Event Name*`
  String get event_name {
    return Intl.message(
      'Event Name*',
      name: 'event_name',
      desc: '',
      args: [],
    );
  }

  /// `Note`
  String get note {
    return Intl.message(
      'Note',
      name: 'note',
      desc: '',
      args: [],
    );
  }

  /// `Type the note here...`
  String get note_detail {
    return Intl.message(
      'Type the note here...',
      name: 'note_detail',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date {
    return Intl.message(
      'Date',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Please enter date.`
  String get date_warning {
    return Intl.message(
      'Please enter date.',
      name: 'date_warning',
      desc: '',
      args: [],
    );
  }

  /// `Start time`
  String get start_time {
    return Intl.message(
      'Start time',
      name: 'start_time',
      desc: '',
      args: [],
    );
  }

  /// `End time`
  String get end_time {
    return Intl.message(
      'End time',
      name: 'end_time',
      desc: '',
      args: [],
    );
  }

  /// `Remind me`
  String get remind_me {
    return Intl.message(
      'Remind me',
      name: 'remind_me',
      desc: '',
      args: [],
    );
  }

  /// `Select Category`
  String get select_category {
    return Intl.message(
      'Select Category',
      name: 'select_category',
      desc: '',
      args: [],
    );
  }

  /// `+ Add new`
  String get add_new {
    return Intl.message(
      '+ Add new',
      name: 'add_new',
      desc: '',
      args: [],
    );
  }

  /// `Add New Category`
  String get add_new_category {
    return Intl.message(
      'Add New Category',
      name: 'add_new_category',
      desc: '',
      args: [],
    );
  }

  /// `Category Name`
  String get category_name {
    return Intl.message(
      'Category Name',
      name: 'category_name',
      desc: '',
      args: [],
    );
  }

  /// `Pick a color!`
  String get pick_color {
    return Intl.message(
      'Pick a color!',
      name: 'pick_color',
      desc: '',
      args: [],
    );
  }

  /// `DONE`
  String get done {
    return Intl.message(
      'DONE',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `Create Event`
  String get create_event {
    return Intl.message(
      'Create Event',
      name: 'create_event',
      desc: '',
      args: [],
    );
  }

  /// `profile`
  String get profile {
    return Intl.message(
      'profile',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `My account`
  String get my_account {
    return Intl.message(
      'My account',
      name: 'my_account',
      desc: '',
      args: [],
    );
  }

  /// `change pass`
  String get change_pass {
    return Intl.message(
      'change pass',
      name: 'change_pass',
      desc: '',
      args: [],
    );
  }

  /// `change pin code`
  String get change_pin_code {
    return Intl.message(
      'change pin code',
      name: 'change_pin_code',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get setting {
    return Intl.message(
      'Setting',
      name: 'setting',
      desc: '',
      args: [],
    );
  }

  /// `Log out`
  String get log_out {
    return Intl.message(
      'Log out',
      name: 'log_out',
      desc: '',
      args: [],
    );
  }

  /// `More`
  String get more {
    return Intl.message(
      'More',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get Share {
    return Intl.message(
      'Share',
      name: 'Share',
      desc: '',
      args: [],
    );
  }

  /// `Evaluation`
  String get evaluation {
    return Intl.message(
      'Evaluation',
      name: 'evaluation',
      desc: '',
      args: [],
    );
  }

  /// `Help & Support`
  String get help_support {
    return Intl.message(
      'Help & Support',
      name: 'help_support',
      desc: '',
      args: [],
    );
  }

  /// `About App`
  String get about_app {
    return Intl.message(
      'About App',
      name: 'about_app',
      desc: '',
      args: [],
    );
  }

  /// `Bio-data`
  String get bio_data {
    return Intl.message(
      'Bio-data',
      name: 'bio_data',
      desc: '',
      args: [],
    );
  }

  /// `What's your first name?`
  String get what_your_first_name {
    return Intl.message(
      'What\'s your first name?',
      name: 'what_your_first_name',
      desc: '',
      args: [],
    );
  }

  /// `And your last name?`
  String get and_your_last_name {
    return Intl.message(
      'And your last name?',
      name: 'and_your_last_name',
      desc: '',
      args: [],
    );
  }

  /// `Email?`
  String get email {
    return Intl.message(
      'Email?',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Select Your Gender`
  String get select_your_gender {
    return Intl.message(
      'Select Your Gender',
      name: 'select_your_gender',
      desc: '',
      args: [],
    );
  }

  /// `What is your date of bith?`
  String get what_is_your_date_of_bith {
    return Intl.message(
      'What is your date of bith?',
      name: 'what_is_your_date_of_bith',
      desc: '',
      args: [],
    );
  }

  /// `Update Profile`
  String get update_profile {
    return Intl.message(
      'Update Profile',
      name: 'update_profile',
      desc: '',
      args: [],
    );
  }

  /// `Please select gender.`
  String get please_select_gender {
    return Intl.message(
      'Please select gender.',
      name: 'please_select_gender',
      desc: '',
      args: [],
    );
  }

  /// `Male`
  String get male {
    return Intl.message(
      'Male',
      name: 'male',
      desc: '',
      args: [],
    );
  }

  /// `Female`
  String get female {
    return Intl.message(
      'Female',
      name: 'female',
      desc: '',
      args: [],
    );
  }

  /// `Other`
  String get other {
    return Intl.message(
      'Other',
      name: 'other',
      desc: '',
      args: [],
    );
  }

  /// `Enter PIN`
  String get pin_code_title {
    return Intl.message(
      'Enter PIN',
      name: 'pin_code_title',
      desc: '',
      args: [],
    );
  }

  /// `Forgot`
  String get pin_code_forgot {
    return Intl.message(
      'Forgot',
      name: 'pin_code_forgot',
      desc: '',
      args: [],
    );
  }

  /// `Tap here`
  String get pin_code_tap_here {
    return Intl.message(
      'Tap here',
      name: 'pin_code_tap_here',
      desc: '',
      args: [],
    );
  }

  /// `Enter old pin code`
  String get enter_old_pin_code {
    return Intl.message(
      'Enter old pin code',
      name: 'enter_old_pin_code',
      desc: '',
      args: [],
    );
  }

  /// `Enter new pin code`
  String get enter_new_pin_code {
    return Intl.message(
      'Enter new pin code',
      name: 'enter_new_pin_code',
      desc: '',
      args: [],
    );
  }

  /// `Confirm new pin code`
  String get confirm_new_pin_code {
    return Intl.message(
      'Confirm new pin code',
      name: 'confirm_new_pin_code',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'vi'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
