// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a vi locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'vi';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Share": MessageLookupByLibrary.simpleMessage("Chia sẻ"),
        "about_app":
            MessageLookupByLibrary.simpleMessage("Thông tin về ứng dụng"),
        "add_new": MessageLookupByLibrary.simpleMessage("+ Thêm mới"),
        "add_new_category":
            MessageLookupByLibrary.simpleMessage("Thêm loại sự kiện mới"),
        "add_new_event":
            MessageLookupByLibrary.simpleMessage("Thêm sự kiện mới"),
        "and_your_last_name": MessageLookupByLibrary.simpleMessage("Nhập Tên?"),
        "bio_data": MessageLookupByLibrary.simpleMessage("Hồ sơ"),
        "category_name":
            MessageLookupByLibrary.simpleMessage("Tên loại sự kiện"),
        "change_pass": MessageLookupByLibrary.simpleMessage("Đổi mật khẩu"),
        "change_pin_code":
            MessageLookupByLibrary.simpleMessage("Đổi mật khẩu pin code"),
        "confirm_new_pin_code": MessageLookupByLibrary.simpleMessage(
            "Xác nhận lại mã Pin mới"),
        "create_event": MessageLookupByLibrary.simpleMessage("Tạo sự kiện"),
        "date": MessageLookupByLibrary.simpleMessage("Ngày/Tháng/Năm"),
        "date_warning":
            MessageLookupByLibrary.simpleMessage("Vui lòng nhập ngày"),
        "done": MessageLookupByLibrary.simpleMessage("XONG"),
        "email": MessageLookupByLibrary.simpleMessage("Email?"),
        "end_time": MessageLookupByLibrary.simpleMessage("Thời gian kết thúc"),
        "enter_new_pin_code":
            MessageLookupByLibrary.simpleMessage("Nhập mã Pin mới"),
        "enter_old_pin_code":
            MessageLookupByLibrary.simpleMessage("Nhập mã Pin cũ"),
        "evaluation": MessageLookupByLibrary.simpleMessage("Đánh giá"),
        "event_name": MessageLookupByLibrary.simpleMessage("Tên sự kiện*"),
        "female": MessageLookupByLibrary.simpleMessage("Nữ"),
        "help_support": MessageLookupByLibrary.simpleMessage("Trợ giúp"),
        "log_out": MessageLookupByLibrary.simpleMessage("Đăng xuất"),
        "male": MessageLookupByLibrary.simpleMessage("Nam"),
        "more": MessageLookupByLibrary.simpleMessage("Hơn nữa"),
        "my_account": MessageLookupByLibrary.simpleMessage("Tài khoản của tôi"),
        "note": MessageLookupByLibrary.simpleMessage("Ghi chú"),
        "note_detail": MessageLookupByLibrary.simpleMessage(
            "Nhập ghi chú của bạn ở đây..."),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "other": MessageLookupByLibrary.simpleMessage("Khác"),
        "pick_color":
            MessageLookupByLibrary.simpleMessage("Chọn 1 màu đại diện!"),
        "pin_code_forgot": MessageLookupByLibrary.simpleMessage("Quên?"),
        "pin_code_tap_here":
            MessageLookupByLibrary.simpleMessage("nhấn vào đây."),
        "pin_code_title": MessageLookupByLibrary.simpleMessage("Nhập PIN"),
        "please_select_gender":
            MessageLookupByLibrary.simpleMessage("Vui lòng chọn giới tính."),
        "profile": MessageLookupByLibrary.simpleMessage("Hồ sơ của tôi"),
        "remind_me": MessageLookupByLibrary.simpleMessage("Nhắc nhở tôi"),
        "select_category":
            MessageLookupByLibrary.simpleMessage("Chọn loại sự kiện"),
        "select_your_gender":
            MessageLookupByLibrary.simpleMessage("Giới tính?"),
        "setting": MessageLookupByLibrary.simpleMessage("Cài đặt"),
        "start_time": MessageLookupByLibrary.simpleMessage("Thời gian bắt đầu"),
        "update_profile":
            MessageLookupByLibrary.simpleMessage("Cập nhật hồ sơ"),
        "what_is_your_date_of_bith":
            MessageLookupByLibrary.simpleMessage("Nhập ngày sinh?"),
        "what_your_first_name":
            MessageLookupByLibrary.simpleMessage("Nhập Họ và tên đệm?")
      };
}
