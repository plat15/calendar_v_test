// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Share": MessageLookupByLibrary.simpleMessage("Share"),
        "about_app": MessageLookupByLibrary.simpleMessage("About App"),
        "add_new": MessageLookupByLibrary.simpleMessage("+ Add new"),
        "add_new_category":
            MessageLookupByLibrary.simpleMessage("Add New Category"),
        "add_new_event": MessageLookupByLibrary.simpleMessage("Add New Event"),
        "and_your_last_name":
            MessageLookupByLibrary.simpleMessage("And your last name?"),
        "bio_data": MessageLookupByLibrary.simpleMessage("Bio-data"),
        "category_name": MessageLookupByLibrary.simpleMessage("Category Name"),
        "change_pass": MessageLookupByLibrary.simpleMessage("change pass"),
        "change_pin_code":
            MessageLookupByLibrary.simpleMessage("change pin code"),
        "confirm_new_pin_code":
            MessageLookupByLibrary.simpleMessage("Confirm new pin code"),
        "create_event": MessageLookupByLibrary.simpleMessage("Create Event"),
        "date": MessageLookupByLibrary.simpleMessage("Date"),
        "date_warning":
            MessageLookupByLibrary.simpleMessage("Please enter date."),
        "done": MessageLookupByLibrary.simpleMessage("DONE"),
        "email": MessageLookupByLibrary.simpleMessage("Email?"),
        "end_time": MessageLookupByLibrary.simpleMessage("End time"),
        "enter_new_pin_code":
            MessageLookupByLibrary.simpleMessage("Enter new pin code"),
        "enter_old_pin_code":
            MessageLookupByLibrary.simpleMessage("Enter old pin code"),
        "evaluation": MessageLookupByLibrary.simpleMessage("Evaluation"),
        "event_name": MessageLookupByLibrary.simpleMessage("Event Name*"),
        "female": MessageLookupByLibrary.simpleMessage("Female"),
        "help_support": MessageLookupByLibrary.simpleMessage("Help & Support"),
        "log_out": MessageLookupByLibrary.simpleMessage("Log out"),
        "male": MessageLookupByLibrary.simpleMessage("Male"),
        "more": MessageLookupByLibrary.simpleMessage("More"),
        "my_account": MessageLookupByLibrary.simpleMessage("My account"),
        "note": MessageLookupByLibrary.simpleMessage("Note"),
        "note_detail":
            MessageLookupByLibrary.simpleMessage("Type the note here..."),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "other": MessageLookupByLibrary.simpleMessage("Other"),
        "pick_color": MessageLookupByLibrary.simpleMessage("Pick a color!"),
        "pin_code_forgot": MessageLookupByLibrary.simpleMessage("Forgot"),
        "pin_code_tap_here": MessageLookupByLibrary.simpleMessage("Tap here"),
        "pin_code_title": MessageLookupByLibrary.simpleMessage("Enter PIN"),
        "please_select_gender":
            MessageLookupByLibrary.simpleMessage("Please select gender."),
        "profile": MessageLookupByLibrary.simpleMessage("profile"),
        "remind_me": MessageLookupByLibrary.simpleMessage("Remind me"),
        "select_category":
            MessageLookupByLibrary.simpleMessage("Select Category"),
        "select_your_gender":
            MessageLookupByLibrary.simpleMessage("Select Your Gender"),
        "setting": MessageLookupByLibrary.simpleMessage("Setting"),
        "start_time": MessageLookupByLibrary.simpleMessage("Start time"),
        "update_profile":
            MessageLookupByLibrary.simpleMessage("Update Profile"),
        "what_is_your_date_of_bith":
            MessageLookupByLibrary.simpleMessage("What is your date of bith?"),
        "what_your_first_name":
            MessageLookupByLibrary.simpleMessage("What\'s your first name?")
      };
}
