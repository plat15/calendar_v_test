import 'package:flutter/material.dart';

@immutable
class Constants {
  static String baseURL = "http://asiasoftdn.ddns.net:823/00008/service.asmx";
  static String KEY = "9c31c9bb55f379db5988e9ea16a77cff";
  static const String versionAPI = "/api/v1";
  static const String versionPath = "/api/v1";
  static const String v2Path = "/api/v2";
  static const String defaultUrlWebView = "https://";
  static const String defaultUrlImage = "";
  static String USERNAME = "001_XE001";
  static String PASS = "1";
}
