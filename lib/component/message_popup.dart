import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/styles.dart';

class MessageDialog extends StatelessWidget {
  final String message;
  final VoidCallback? onTap;

  static Future show(BuildContext context, String message, {VoidCallback? onTap}) {
    return showDialog(
        context: context, barrierDismissible: false, builder: (_) => MessageDialog(message: message, onTap: onTap));
  }

  const MessageDialog({Key? key, required this.message, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: Container(
        constraints: const BoxConstraints(minWidth: 280),
        child: Wrap(
          children: [
            Column(
              children: [
                const SizedBox(height : 40),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    message,
                    style: Styles.n14.copyWith(color: AppColors.h333333, height: 1.4),
                    textAlign: TextAlign.center,
                  ),
                ),
                InkWell(
                  onTap: () {
                    onTap?.call();
                    Navigator.pop(context);
                  },
                  child: SizedBox(
                    height: 75,
                    child: Center(
                      child: Text("Close",
                          style: Styles.n16.copyWith(color: AppColors.h0061FF, fontWeight: FontWeight.w600)),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
