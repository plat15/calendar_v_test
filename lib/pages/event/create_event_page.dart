import 'package:calendar_flutter/base/base_builder.dart';
import 'package:calendar_flutter/generated/l10n.dart';
import 'package:calendar_flutter/pages/event/create_event_viewmodel.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:intl/intl.dart';

class CreateEventWidget extends StatefulWidget {
  const CreateEventWidget({super.key});

  @override
  State<CreateEventWidget> createState() => _CreateEventWidgetState();
}

class _CreateEventWidgetState extends State<CreateEventWidget> {
  final _provider = EventVM();

  @override
  void initState() {
    _provider.startTimeController.text = "";
    _provider.endTimeController.text = "";
    super.initState();
  }

  @override
  void dispose() {
    _provider.myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseBuilder<EventVM>(
      model: _provider,
      child: Container(
        padding: const EdgeInsets.all(10),
        height: 650,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              S.of(context).add_new_event,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            const SizedBox(height: 10),
            TextFormField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: const BorderSide(width: 3, color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                hintText: S.of(context).event_name,
                label: Text(S.of(context).event_name),
              ),
            ),
            const SizedBox(height: 10),
            TextFormField(
              maxLines: 2,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: const BorderSide(width: 3, color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                hintText: S.of(context).note_detail,
                label: Text(S.of(context).note),
              ),
            ),
            const SizedBox(height: 10),
            TextFormField(
              readOnly: true,
              controller: _provider.dateController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide:
                          const BorderSide(width: 3, color: Colors.grey),
                      borderRadius: BorderRadius.circular(15)),
                  label: Text(S.of(context).date),
                  suffixIcon: const Icon(Icons.calendar_today)),
              onTap: () async {
                await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2015),
                  lastDate: DateTime(2025),
                ).then((selectedDate) {
                  if (selectedDate != null) {
                    _provider.dateController.text =
                        DateFormat('dd/MM/yyyy').format(selectedDate);
                  }
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return S.of(context).date_warning;
                }
                return null;
              },
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Container(
                    child: TextFormField(
                      controller: _provider.startTimeController,
                      readOnly: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  width: 3, color: Colors.grey),
                              borderRadius: BorderRadius.circular(15)),
                          label: Text(S.of(context).start_time),
                          suffixIcon: const Icon(Icons.alarm)),
                      onTap: () async {
                        TimeOfDay? pickedTime = await showTimePicker(
                          initialTime: TimeOfDay.now(),
                          context: context,
                        );
                        setState(() {
                          _provider.startTimeController.text =
                              pickedTime!.format(context);
                        });
                      },
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Container(
                    child: TextFormField(
                      controller: _provider.endTimeController,
                      readOnly: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  width: 3, color: Colors.grey),
                              borderRadius: BorderRadius.circular(15)),
                          label: Text(S.of(context).end_time),
                          suffixIcon: Icon(Icons.alarm)),
                      onTap: () async {
                        TimeOfDay? pickedTime = await showTimePicker(
                          initialTime: TimeOfDay.now(),
                          context: context,
                        );
                        setState(() {
                          _provider.endTimeController.text =
                              pickedTime!.format(context);
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Text(
                  S.of(context).remind_me,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const Spacer(),
                Switch(
                    value: _provider.light,
                    activeColor: AppColors.h735BF2,
                    onChanged: (bool value) {
                      setState(() {
                        _provider.light = value;
                      });
                    })
              ],
            ),
            const SizedBox(height: 10),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                S.of(context).select_category,
                style: TextStyle(fontSize: 20),
              ),
            ),
            const SizedBox(height: 10),
            Container(
              height: 50,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _provider.listCategory.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Row(
                      children: [
                        Container(
                          height: 50,
                          width: 140,
                          child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                backgroundColor:
                                    _provider.expandedIndex == index
                                        ? _provider.listColor[index]
                                        : Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                              onPressed: () {
                                setState(() {
                                  _provider.expandedIndex = index;
                                });
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.check_circle,
                                      size: 20,
                                      color: _provider.expandedIndex == index
                                          ? Colors.grey
                                          : _provider.listColor[index],
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      _provider.listCategory[index],
                                      style: TextStyle(
                                          fontSize: 12,
                                          color:
                                              _provider.expandedIndex == index
                                                  ? Colors.white
                                                  : Colors.black),
                                    ),
                                  ])),
                        ),
                        const SizedBox(
                          width: 5,
                        )
                      ],
                    );
                  }),
            ),
            const SizedBox(
              height: 10,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: InkWell(
                child: Text(
                  S.of(context).add_new,
                  style: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: AppColors.h735BF2,
                      decoration: TextDecoration.underline,
                      decorationColor: AppColors.h735BF2),
                ),
                onTap: () => showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                          title: Text(S.of(context).add_new_category),
                          actions: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: _provider.myController,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              width: 3, color: Colors.grey),
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      hintText: S.of(context).category_name,
                                    ),
                                  ),
                                  ElevatedButton(
                                    onPressed: () {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: Text(
                                                  S.of(context).pick_color),
                                              content: SingleChildScrollView(
                                                child: ColorPicker(
                                                  pickerColor:
                                                      _provider.currentColor,
                                                  onColorChanged:
                                                      (Color color) {
                                                    setState(() {
                                                      _provider.currentColor =
                                                          color;
                                                    });
                                                  },
                                                ),
                                              ),
                                              actions: <Widget>[
                                                ElevatedButton(
                                                  child:
                                                      Text(S.of(context).done),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                              ],
                                            );
                                          });
                                    },
                                    child: Text(S.of(context).pick_color),
                                  ),
                                  ElevatedButton(
                                    child: Text(S.of(context).ok),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      if (_provider.myController.text != '') {
                                        _provider.listCategory
                                            .add(_provider.myController.text);
                                        _provider.listColor
                                            .add(_provider.currentColor);
                                      }
                                      _provider.myController.clear();
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: double.infinity,
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    backgroundColor: AppColors.h735BF2),
                onPressed: () {
                  Navigator.pop(context, 'ok');
                },
                child: Text(
                  S.of(context).create_event,
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
