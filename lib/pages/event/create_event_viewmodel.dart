import 'package:calendar_flutter/base/base_viewmodel.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';

class EventVM extends BaseViewModel {
  Color pickerColor = Color(0xff443a49);
  Color currentColor = Color(0xff443a49);

  final myController = TextEditingController();
  final dateController = TextEditingController();
  final startTimeController = TextEditingController();
  final endTimeController = TextEditingController();

  int? expandedIndex;

  final listCategory = <String>['Brainstorm', 'Design', 'Workout'];
  final listColor = <Color>[
    AppColors.h735BF2,
    AppColors.h59C36A,
    AppColors.h0D5F95
  ];

  bool light = false;
}
