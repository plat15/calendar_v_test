import 'package:flutter/material.dart';

import '../introduc/introduc_page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashState();
}

class _SplashState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    _navigatetohome();
  }

  _navigatetohome() async {
    await Future.delayed(const Duration(milliseconds: 2500), () {});
    // ignore: use_build_context_synchronously
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => const IntroducPage(
            // title: "Calender",
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "MY CALENDAR",
                style: TextStyle(
                  fontSize: 50,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.italic,
                  color: Colors.black,
                  decoration: TextDecoration.underline,
                ),
              ),
              const Text(
                "ACTUALLT YOURS",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.italic,
                  // color: Colors.black,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(5, 5),
                      blurRadius: 3.0,
                      color: Colors.black12,
                    )
                  ],
                ),
              ),
              Image.asset(
                'assets/images/image_splash_png.png',
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
