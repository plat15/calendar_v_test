import 'package:calendar_flutter/base/base_viewmodel.dart';
import 'package:calendar_flutter/pages/calendar/calendar_page.dart';
import 'package:calendar_flutter/pages/my_page/my_page_page.dart';
import 'package:flutter/material.dart';

class HomeVM extends BaseViewModel {
  List<IconData> iconList = [
    Icons.calendar_today,
    Icons.alarm,
    Icons.notifications,
    Icons.person
  ];
  int bottomNavIndex = 0;

  List<Widget> widgetOptions = [];

  HomeVM() {
    widgetOptions = [
      const CalenderPage(),
      const Center(
        child: Text(
          'Index 1: Remind',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
      ),
      const Center(
        child: Text(
          'Index 2: Notification',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
      ),
      const MyPage()
    ];
  }

  void onItemTapped(int index) {
    bottomNavIndex = index;
    notifyListeners();
  }
}
