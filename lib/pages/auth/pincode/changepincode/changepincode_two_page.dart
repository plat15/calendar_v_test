import 'package:calendar_flutter/base/base_builder.dart';
import 'package:calendar_flutter/gen/assets.gen.dart';
import 'package:calendar_flutter/generated/l10n.dart';
import 'package:calendar_flutter/pages/auth/pincode/EnterPincode/pincode_viewmodel.dart';
import 'package:calendar_flutter/pages/auth/pincode/component/list_number.dart';
import 'package:calendar_flutter/pages/auth/pincode/component/pincode_number.dart';
import 'package:flutter/material.dart';

class ChangePinCodeTwo extends StatefulWidget {
  const ChangePinCodeTwo({super.key});
  @override
  State<ChangePinCodeTwo> createState() => _ChangePinCodeTwoState();
}

class _ChangePinCodeTwoState extends State<ChangePinCodeTwo> {
  final _provider = PinCodeVM();
  @override
  Widget build(BuildContext context) {
    return BaseBuilder<PinCodeVM>(
      model: _provider,
      child: Scaffold(
        body: SafeArea(
          // Background
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(Assets.images.backgroundpincode.path),
                  fit: BoxFit.cover),
            ),
            child: Center(
              child: Stack(
                children: [
                  Positioned(
                    top: 20.0,
                    left: 20.0,
                    child: IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.arrow_back,
                        size: 30.0,
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(height: 100.0),
                      Text(
                        S.of(context).enter_new_pin_code,
                        style: const TextStyle(
                          fontFamily: 'IBM Plex Serif',
                          fontWeight: FontWeight.w400,
                          fontSize: 30.0,
                        ),
                      ),
                      const SizedBox(height: 75.0),
                      PinCodeNumber(
                        provider: _provider,
                      ),
                      const SizedBox(height: 100.0),
                      Expanded(
                        child: ListNumber(
                          provider: _provider,
                          type: Type.confirm,                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
