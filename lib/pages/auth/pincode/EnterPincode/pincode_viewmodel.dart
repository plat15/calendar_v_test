import 'package:calendar_flutter/base/base_viewmodel.dart';
import 'package:calendar_flutter/remote/local/shared_prefs.dart';
import 'package:flutter/material.dart';

enum Type { enter, old, confirm, done }

class PinCodeVM extends BaseViewModel {
  // pinCodeResult dc lấy từ local , nếu mới vô app lần đầu thì call api để lấy
  // Sử dụng SharedPrefs để lưu ở local
  String? pinCodeResult = '0103';
  SharedPrefs sharedPrefs = SharedPrefs();
  TextEditingController controller = TextEditingController(text: '');
  String? text = '';
  final List<String> list = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'x',
    '0',
    '',
  ];
  @override
  void onInit() {
    //  getPinCodeResuat();
  }

  void addPincode(String? s) {
    if (text!.length < 4) {
      text = text! + s!;
      controller.text = text ?? '';
    }
    notifyListeners();
  }

  void clearnState() {
    controller.clear();
  }

  String removeCharFromRight(String inputString, int numCharsToRemove) {
    if (numCharsToRemove <= 0) {
      return inputString;
    }

    int endIndex = inputString.length - numCharsToRemove;
    if (endIndex <= 0) {
      return "";
    }

    return inputString.substring(0, endIndex);
  }

  void remove() {
    if (text!.length > 0 || text!.length <= 4) {
      text = removeCharFromRight(text!, 1);
      controller.text = text ?? '';
    }
    notifyListeners();
  }

  bool checkPinCode(String? s) {
    if (pinCodeResult == s) {
      return true;
    }
    return false;
  }

  void getPinCodeResuat() async {
    await sharedPrefs.initialise();
    pinCodeResult = sharedPrefs.pincode ?? pinCodeResult;
  }

  void checkPincode() {
    if (controller.text.length == 4) {
      if (checkPinCode(controller.text)) {
        print('PIN CODE SUCCESS');
        notifyListeners();
      } else {
        print('PIN CODE FAILSE');
      }
    }
  }

  void checkPincodeOld() {}

  void confirmPincode() {}

  void changeCodePin() {}

  handerPincode(Type type) {
    switch (type) {
      case Type.enter:
        return checkPincode();
      case Type.old:
        return checkPincodeOld();
      case Type.confirm:
        return confirmPincode();
      case Type.done:
        return changeCodePin();
    }
  }
}
