import 'package:calendar_flutter/base/base_builder.dart';
import 'package:calendar_flutter/gen/assets.gen.dart';
import 'package:calendar_flutter/generated/l10n.dart';
import 'package:calendar_flutter/pages/auth/pincode/EnterPincode/pincode_viewmodel.dart';
import 'package:calendar_flutter/pages/auth/pincode/component/list_number.dart';
import 'package:calendar_flutter/pages/auth/pincode/component/pincode_number.dart';
import 'package:flutter/material.dart';

class PinCodePage extends StatefulWidget {
  const PinCodePage({super.key});
  @override
  State<PinCodePage> createState() => _PiCodePageState();
}

class _PiCodePageState extends State<PinCodePage> {
  @override
  void dispose() {
    _provider.clearnState();
    super.dispose();
  }

  final _provider = PinCodeVM();
  @override
  Widget build(BuildContext context) {
    return BaseBuilder<PinCodeVM>(
      model: _provider,
      child: Scaffold(
        body: SafeArea(
          // Background
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(Assets.images.backgroundpincode.path),
                  fit: BoxFit.cover),
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(height: 100.0),
                  Text(
                    S.of(context).pin_code_title,
                    style: const TextStyle(
                      fontFamily: 'IBM Plex Serif',
                      fontWeight: FontWeight.w400,
                      fontSize: 30.0,
                    ),
                  ),
                  const SizedBox(height: 75.0),
                  PinCodeNumber(
                    provider: _provider,
                  ),
                  const SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${S.of(context).pin_code_forgot} ',
                        style: const TextStyle(
                          fontFamily: 'IBM Plex Serif',
                          color: Colors.black,
                          fontSize: 16.0,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          S.of(context).pin_code_tap_here,
                          style: const TextStyle(
                            fontFamily: 'IBM Plex Serif',
                            color: Colors.black,
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 60.0),
                  Expanded(
                    child: ListNumber(
                      provider: _provider,
                      type: Type.enter,                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
