import 'package:flutter/material.dart';

class PinCodePageWidget extends StatelessWidget {
  const PinCodePageWidget(
      {super.key,
      this.namePin,
      required this.ontap1,
      required this.ontap2,
      required this.ontap3});
  final String? namePin;
  final void Function() ontap1;
  final void Function() ontap2;
  final void Function() ontap3;

  @override
  Widget build(BuildContext context) {
    double fem = MediaQuery.of(context).size.width;
    double femme = MediaQuery.of(context).size.height;
    return Container(
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            blurRadius: 5,
            color: Colors.grey,
            offset: Offset(4, 4),
          ),
        ],
      ),
      child: GestureDetector(
        onTap: namePin!.isEmpty
            ? ontap3
            : namePin!.contains('x')
                ? ontap1
                : ontap2,
        child: Container(
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: const Color.fromRGBO(245, 144, 146, 1),
          ),
          child: Center(
            child: namePin!.isNotEmpty
                ? Text(
                    namePin!,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15 * femme / fem,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'IBM Plex Serif',
                      shadows: const [
                        Shadow(
                          offset: Offset(5, 4),
                          blurRadius: 8,
                          color: Color.fromRGBO(77, 77, 77, 0.5),
                        ),
                      ],
                    ),
                  )
                : const Icon(Icons.done),
          ),
        ),
      ),
    );
  }
}
