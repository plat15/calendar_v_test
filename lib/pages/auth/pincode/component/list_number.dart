import 'package:calendar_flutter/pages/auth/pincode/EnterPincode/pincode_viewmodel.dart';
import 'package:calendar_flutter/pages/auth/pincode/Widget/button_number_widget.dart';
import 'package:flutter/material.dart';

class ListNumber extends StatefulWidget {
  final PinCodeVM provider;
  final Type type;
  const ListNumber({
    super.key,
    required this.provider,
    required this.type,
  });

  @override
  State<ListNumber> createState() => _ListNumberState();
}

class _ListNumberState extends State<ListNumber> {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 3,
      padding: const EdgeInsets.only(left: 50.0, right: 50.0, top: 70),
      crossAxisSpacing: 45,
      mainAxisSpacing: 25,
      children: widget.provider.list
          .map(
            (e) => PinCodePageWidget(
              namePin: e,
              ontap2: () {
                widget.provider.addPincode(e);
              },
              ontap1: () {
                widget.provider.remove();
              },
              ontap3: () {
                widget.provider.handerPincode(widget.type);
              },
            ),
          )
          .toList(),
    );
  }
}
