import 'package:calendar_flutter/pages/auth/pincode/EnterPincode/pincode_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';

class PinCodeNumber extends StatefulWidget {
  final PinCodeVM provider;
  const PinCodeNumber(
      {super.key, required this.provider});

  @override
  State<PinCodeNumber> createState() => _PinCodeNumberState();
}

class _PinCodeNumberState extends State<PinCodeNumber> {
  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 16,
      height: 16,
      decoration: BoxDecoration(
        color: const Color.fromRGBO(245, 144, 146, 1),
        borderRadius: BorderRadius.circular(20),
      ),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration?.copyWith(
        color: const Color.fromRGBO(245, 144, 146, 1),
        border: Border.all(
          width: 2.0,
          color: const Color.fromRGBO(246, 247, 248, 1),
        ),
      ),
    );
    return Pinput(
      controller: widget.provider.controller,
      length: 4,
      defaultPinTheme: defaultPinTheme,
      submittedPinTheme: submittedPinTheme,
      obscureText: true,
      obscuringCharacter: r' ',
      showCursor: false,
      readOnly: true,
      onChanged: (s) {},
    );
  }
}
