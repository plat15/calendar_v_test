import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:calendar_flutter/pages/event/create_event_page.dart';
import 'package:calendar_flutter/pages/home_viewmodel.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';

import '../base/base_builder.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  final _provider = HomeVM();

  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _provider.bottomNavIndex = _tabController.index;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseBuilder<HomeVM>(
      model: _provider,
      child: Scaffold(
        body: TabBarView(
            controller: _tabController, children: _provider.widgetOptions),
        floatingActionButton: FloatingActionButton(
          backgroundColor: AppColors.h735BF2,
          shape: const CircleBorder(),
          onPressed: () {
            showModalBottomSheet<void>(
              isScrollControlled: true,
              context: context,
              builder: (BuildContext context) => CreateEventWidget(),
            );
          },
          child: const Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: AnimatedBottomNavigationBar(
          notchMargin: 20,
          height: 100,
          leftCornerRadius: 20,
          rightCornerRadius: 20,
          activeColor: AppColors.h735BF2,
          gapLocation: GapLocation.center,
          notchSmoothness: NotchSmoothness.verySmoothEdge,
          backgroundColor: Colors.white,
          icons: _provider.iconList,
          activeIndex: _provider.bottomNavIndex,
          onTap: (item) {
            setState(() {
              _tabController.animateTo(item);
            });
            _provider.onItemTapped(item);
          },
        ),
      ),
    );
  }
}
