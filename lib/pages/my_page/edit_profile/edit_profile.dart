import 'package:calendar_flutter/generated/l10n.dart';
import 'package:calendar_flutter/pages/my_page/edit_profile/datepicker_screen.dart';
import 'package:calendar_flutter/pages/my_page/edit_profile/dropdrownbutton.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({super.key});

  @override
  State<EditProfile> createState() => _EditProfileState();
}

List<String> list = <String>['Male', 'Female', 'Other '];

class _EditProfileState extends State<EditProfile> {
  String dropdownValue = list.first;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          centerTitle: true,
          title: Text(
           S.of(context).bio_data,
            style: TextStyle(fontSize: 20),
          ),
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          )),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(children: [
          const SizedBox(
            height: 42,
          ),
          Center(
            child: Container(
              height: 72,
              width: 72,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(72),
                  border: Border.all(
                      width: 3, color: Color.fromARGB(78, 255, 255, 255)),
                  image: const DecorationImage(
                      image: NetworkImage(
                          "https://th.bing.com/th/id/OIP.x7X2oAehk5M9IvGwO_K0PgHaHa?pid=ImgDet&rs=1"),
                      fit: BoxFit.cover)),
            ),
          ),
          const SizedBox(
            height: 21,
          ),
          const Text(
            "Nguyễn Tấn Phiên",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 40,
          ),
          Card(
            child: SizedBox(
              height: 54,
              child: TextFormField(
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 16),
                    border: InputBorder.none,
                    hintText: S.of(context).what_your_first_name,
                    hintStyle: const TextStyle(
                      fontSize: 13,
                    )),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Card(
            child: SizedBox(
              height: 54,
              child: TextFormField(
                decoration:  InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 16),
                    border: InputBorder.none,
                    hintText: S.of(context).and_your_last_name,
                    hintStyle: TextStyle(fontSize: 13)),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Card(
            child: SizedBox(
              height: 54,
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 16),
                    border: InputBorder.none,
                    hintText: S.of(context).email,
                    hintStyle: TextStyle(fontSize: 13)),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Card(
            child: SizedBox(
              width: 350,
              height: 54,
              child: DropdrownButton(),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Card(
            child: SizedBox(
              height: 54,
              child: DatePickerScreen(),
            ),
          ),
          SizedBox(height: 34),
          SizedBox(
            width: 192,
            height: 55,
            child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateColor.resolveWith(
                      (states) => AppColors.h735BF2),
                ),
                onPressed: () {},
                child: const Text(
                  "Update Profile",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                )),
          )
        ]),
      ),
    );
  }
}
