import 'package:calendar_flutter/generated/l10n.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class DropdrownButton extends StatefulWidget {
  const DropdrownButton({super.key});

  @override
  State<DropdrownButton> createState() => _DropdrownButtonState();
}

class _DropdrownButtonState extends State<DropdrownButton> {
  final List<String> genderItems = [
    S.current.male,
     S.current.female,
    S.current.other
  ];

  String? selectedValue;
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField2<String>(
      isExpanded: true,
      decoration:  const InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 16),
        border: InputBorder.none
      ),
      hint:  Text(
        S.of(context).select_your_gender,
        style: TextStyle(fontSize: 14),
      ),
      
      items: genderItems
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return S.of(context).please_select_gender;
        }
        return null;
      },
      onChanged: (value) {
        //Do something when selected item is changed.
      },
      onSaved: (value) {
        selectedValue = value.toString();
      },
      buttonStyleData: const ButtonStyleData(
        padding: EdgeInsets.only(right: 8),
      ),
      iconStyleData: const IconStyleData(
        icon: Icon(
          Icons.arrow_drop_down,
          color: AppColors.h735BF2,
        ),
        iconSize: 24,
      ),
      menuItemStyleData: const MenuItemStyleData(
        padding: EdgeInsets.symmetric(horizontal: 16),
      ),
    );
  }
}
