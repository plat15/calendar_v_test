import 'package:calendar_flutter/generated/l10n.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePickerScreen extends StatefulWidget {
  const DatePickerScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _DatePickerScreenState createState() => _DatePickerScreenState();
}

class _DatePickerScreenState extends State<DatePickerScreen> {
  DateTime _selectedDate = DateTime.now();
  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                decoration:  InputDecoration(
                  contentPadding: EdgeInsets.symmetric(horizontal: 16),
                  hintText: S.of(context).what_is_your_date_of_bith,
                  hintStyle: TextStyle(fontSize: 13),
                  border: InputBorder.none,
                ),
                focusNode: AlwaysDisabledFocusNode(),
                controller: _textEditingController,
              ),
            ),
            GestureDetector(
              onTap: () {
                _selectDate(context);
              },
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(Icons.date_range_outlined,color: AppColors.h735BF2,),
              ),
            ),
          ],

    );
  }

  _selectDate(BuildContext context) async {
    DateTime? newSelectedDate = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2040),
    );

    if (newSelectedDate != null) {
      _selectedDate = newSelectedDate;
      _textEditingController
        ..text = DateFormat("dd/MM/yyyy").format(_selectedDate)
        ..selection = TextSelection.fromPosition(TextPosition(
            offset: _textEditingController.text.length,
            affinity: TextAffinity.upstream));
    }
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
