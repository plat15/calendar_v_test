
import 'package:calendar_flutter/generated/l10n.dart';
import 'package:calendar_flutter/pages/my_page/edit_profile/edit_profile.dart';
import 'package:calendar_flutter/pages/my_page/item_my_page.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyPage extends StatefulWidget {
  const MyPage({super.key});

  @override
  State<MyPage> createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        S.of(context).profile,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      )),
      body: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Card(
              color: AppColors.h735BF2,
              elevation: 5,
              child: SizedBox(
                height: 89,
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(children: [
                    Container(
                      height: 53,
                      width: 53,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(53),
                        border: Border.all(width: 3, color: Colors.white),
                        image: const DecorationImage(
                            image: NetworkImage(
                                "https://th.bing.com/th/id/OIP.x7X2oAehk5M9IvGwO_K0PgHaHa?pid=ImgDet&rs=1"),
                            fit: BoxFit.cover),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 13),
                      child: Text(
                        "Nguyễn Tấn Phiên",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(right: 18.0),
                      child: InkWell(
                        onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile(),)),
                        child: SvgPicture.asset(
                          'assets/icons/edit_profile.svg',
                          height: 24,
                          width: 24,
                        ),
                      ),
                    )
                  ]),
                ),
              ),
            ),
            const SizedBox(height: 18),
             Card(
              elevation: 5,
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    ItemMyPage(icon: Icons.account_circle, text: S.of(context).my_account),
                    ItemMyPage(icon: Icons.security_sharp, text: S.of(context).change_pass),
                    ItemMyPage(
                        icon: Icons.keyboard_alt_outlined,
                        text: S.of(context).change_pin_code
                        ),
                    ItemMyPage(icon: Icons.settings, text: S.of(context).setting),
                    ItemMyPage(icon: Icons.login, text: S.of(context).log_out),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
             Text(
              S.of(context).more,
              style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.black45),
            ),
            const SizedBox(
              height: 10,
            ),
             Expanded(
              child: Card(
                elevation: 5,
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      ItemMyPage(icon: Icons.share, text: S.of(context).Share),
                      ItemMyPage(icon: Icons.assessment, text: S.of(context).evaluation),
                      ItemMyPage(icon: Icons.help, text: S.of(context).help_support),
                      Expanded(
                        child: ItemMyPage(
                            icon: Icons.health_and_safety_sharp, text: S.of(context).about_app),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
