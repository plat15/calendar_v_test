import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';

class ItemMyPage extends StatefulWidget {
  const ItemMyPage({super.key, required this.icon, required this.text});
  final String text;
  final IconData? icon;

  @override
  State<ItemMyPage> createState() => _ItemMyPageState();
}

class _ItemMyPageState extends State<ItemMyPage> {
 
  @override
  Widget build(BuildContext context) {
   
    return Row(
      children: [
        Icon(
          widget.icon,
          color: AppColors.h735BF2,
          size: 30,
        ),
        Padding(
          padding: EdgeInsets.only(left: 16.0),
          child: Text(
            widget.text,
            style: TextStyle(fontSize: 13, color: Colors.black),
          ),
        ),
        const Spacer(),
        IconButton(
          icon: const Icon(Icons.navigate_next_rounded),
          onPressed: () {},
        )
      ],
    );
  }
}
