import 'package:calendar_flutter/pages/calendar/lunar.dart';
import 'package:calendar_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class CalenderPage extends StatefulWidget {
  const CalenderPage({Key? key}) : super(key: key);

  @override
  State<CalenderPage> createState() => _CalenderPageState();
}

class _CalenderPageState extends State<CalenderPage> {
  get day => null;
  DateTime? _selectedDay;
  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: no_leading_underscores_for_local_identifiers
    void _onDaySelected(DateTime selectedDay) {
      setState(() {
        _selectedDay = selectedDay;
      });
    }

    bool isSameDay(DateTime day1, DateTime day2) {
      return day1.year == day2.year &&
          day1.month == day2.month &&
          day1.day == day2.day;
    }

    return SafeArea(
      child: Scaffold(
          body: Column(
        children: [
          TableCalendar(
            rowHeight: 50,
            firstDay: DateTime.utc(2010, 10, 16),
            lastDay: DateTime.utc(2030, 3, 14),
            focusedDay: _selectedDay!,
            headerStyle: const HeaderStyle(
              formatButtonVisible: false,
              titleTextStyle:
                  TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              titleCentered: true,
            ),
            daysOfWeekHeight: 30,
            calendarBuilders: CalendarBuilders(
              outsideBuilder: (context, day, focusedDay) {
                String t = "";
                List<int> al =
                    Lunar.convertSolar2Lunar(day.day, day.month, day.year, 7);
                // ignore: unused_local_variable
                int ngay = al[0];
                // ignore: unused_local_variable
                int thang = al[1];

                // ignore: unrelated_type_equality_checks
                if (ngay == 1) {
                  t = ("$ngay/$thang");
                } else {
                  t = ngay.toString();
                }

                return GestureDetector(
                  onTap: () {
                    _onDaySelected(day);
                  },
                  child: Column(
                    children: [
                      Text(
                        day.day.toString(),
                        style: const TextStyle(
                            fontSize: 14, color: AppColors.h8F9BB3),
                      ),
                      Text(
                        t,
                        style: const TextStyle(
                            fontSize: 10, color: AppColors.h8F9BB3),
                      ),
                    ],
                  ),
                );
              },
              defaultBuilder: (context, day, focusedDay) {
                // ignore: unused_local_variable
                String t = "";
                List<int> al =
                    Lunar.convertSolar2Lunar(day.day, day.month, day.year, 7);
                // ignore: unused_local_variable
                int ngay = al[0];
                // ignore: unused_local_variable
                int thang = al[1];

                // ignore: unrelated_type_equality_checks
                if (ngay == 1) {
                  t = ("$ngay/$thang");
                } else {
                  t = ngay.toString();
                }

                return GestureDetector(
                  onTap: () {
                    _onDaySelected(day);
                  },
                  child: Container(
                    width: 38,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: isSameDay(day, _selectedDay!)
                            ? Colors.blue
                            : Colors.white),
                    child: Column(
                      children: [
                        Text(
                          day.day.toString(),
                          style: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        Text(
                          t,
                          style: const TextStyle(
                            fontSize: 10,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              todayBuilder: (context, day, focusedDay) {
                String t = "";
                List<int> al =
                    Lunar.convertSolar2Lunar(day.day, day.month, day.year, 7);
                // ignore: unused_local_variable
                int ngay = al[0];
                // ignore: unused_local_variable
                int thang = al[1];

                // ignore: unrelated_type_equality_checks
                if (ngay == 1) {
                  t = ("$ngay/$thang");
                } else {
                  t = ngay.toString();
                }
                return Container(
                  width: 38,
                  decoration: BoxDecoration(
                      color: AppColors.h735BF2,
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Column(
                      children: [
                        Text(
                          day.day.toString(),
                          style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        Text(
                          t,
                          style: const TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          const Divider(
            height: 20,
            thickness: 5,
            indent: 179,
            endIndent: 185,
            color: Color(0xffCED3DE),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: 10,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.all(31),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: 10,
                            height: 10,
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius: BorderRadius.circular(10)),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 5),
                            child: Text(
                              "10:00-13:00",
                              style: TextStyle(
                                  fontSize: 12, color: AppColors.h8F9BB3),
                            ),
                          ),
                          const Spacer(),
                          SizedBox(
                              width: 43,
                              height: 15,
                              child: Image.asset('assets/icons/three_dots.png'))
                        ],
                      ),
                      const Text(
                        "Design new UX flow for Michael ",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                        maxLines: 2,
                      ),
                      const Text(
                        "Start from screen 16",
                        style:
                            TextStyle(fontSize: 12, color: AppColors.h8F9BB3),
                      )
                    ],
                  ),
                );
              },
            ),
          )
        ],
      )),
    );
  }
}
