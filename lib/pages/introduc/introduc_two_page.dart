import 'package:calendar_flutter/pages/auth/pincode/EnterPincode/pincode_page.dart';
import 'package:flutter/material.dart';

class IntroducTwoPage extends StatefulWidget {
  const IntroducTwoPage({super.key});

  @override
  State<IntroducTwoPage> createState() => _IntroducTwoPageState();
}

class _IntroducTwoPageState extends State<IntroducTwoPage> {
  @override
  void initState() {
    super.initState();
    _navigatetohome();
  }

  _navigatetohome() async {
    await Future.delayed(const Duration(milliseconds: 2500), () {});
    // ignore: use_build_context_synchronously
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => const PinCodePage(
            // title: "Calender",
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Text(
              'An app that cares',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 35.0,
                decoration: TextDecoration.underline,
              ),
            ),
            Expanded(
              child: Image.asset(
                'assets/images/image_introduc_two_png.png',
                fit: BoxFit.cover,
              ),
            ),
            Row(
              children: [
                const Icon(Icons.arrow_right_rounded),
                Expanded(
                  child: RichText(
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 19.0,
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: 'Manage your weight ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(text: 'with ecpert guidance'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            Row(
              children: [
                const Icon(Icons.arrow_right_rounded),
                Expanded(
                  child: RichText(
                    overflow: TextOverflow.clip,
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 19.0,
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: 'Train live  ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(text: 'with the best coaches& instuctors'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            Row(
              children: [
                const Icon(Icons.arrow_right_rounded),
                Expanded(
                  child: RichText(
                    overflow: TextOverflow.clip,
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 19.0,
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: 'Ger medical advice ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                            text:
                                'from India top PCOS, Thyroid & Fertility Experts'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            Row(
              children: [
                const Icon(Icons.arrow_right_rounded),
                Expanded(
                  child: RichText(
                    overflow: TextOverflow.clip,
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 19.0,
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: 'Track your periods ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(text: '& other symptoms'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            Row(
              children: [
                const Icon(Icons.arrow_right_rounded),
                Expanded(
                  child: RichText(
                    overflow: TextOverflow.clip,
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 19.0,
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(text: 'Comfortably '),
                        TextSpan(
                          text: 'get lab tests ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(text: 'from your home'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
