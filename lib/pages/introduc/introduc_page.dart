import 'package:calendar_flutter/pages/introduc/introduc_two_page.dart';
import 'package:flutter/material.dart';

class IntroducPage extends StatefulWidget {
  const IntroducPage({super.key});

  @override
  State<IntroducPage> createState() => _IntroducPageState();
}

class _IntroducPageState extends State<IntroducPage> {
  @override
  void initState() {
    super.initState();
    _navigatetohome();
  }

  _navigatetohome() async {
    await Future.delayed(const Duration(milliseconds: 2500), () {});
    // ignore: use_build_context_synchronously
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => const IntroducTwoPage(
            // title: "Calender",
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Stack(
          fit: StackFit.expand,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                RichText(
                  text: const TextSpan(
                    style: TextStyle(
                      fontSize: 45.0,
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                    children: [
                      TextSpan(text: 'Personalised, science- backed care for '),
                      TextSpan(
                        text: 'PCOS& Thyroid',
                        style: TextStyle(
                          color: Colors.red,
                          decoration: TextDecoration.underline,
                          decorationColor: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 50.0),
                const Text(
                  "Navigating weight gain, period issues,hair loss or fertility concerns can be overwhelmingly hard.",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w100,
                  ),
                ),
                const SizedBox(height: 120.0),
                const Text(
                  'We gat it.',
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.w100,
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 50.0,
              right: 50.0,
              left: 50.0,
              child: Image.asset(
                'assets/images/image_introduc_png.png',
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
