import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../models/account_model.dart';

class SharedPrefs {
  static const String _PROFILE = '_PROFILE';
  static const String _FCM_TOKEN = 'JUMNAVI_FCM_TOKEN';
   static const String _BASE_URL = 'BASE_URL_SERVER';
      static const String _KEY_URL = 'KEY_URL_SERVER';
  static const String _WELCOME = 'WELCOME_VERSION_PHASE_ONE';
  static const String _SHOW_DIALOG_TUTORIAL = '_SHOW_DIALOG_TUTORIAL';
  static const String _SHOULD_SHOW_TARGETED_EVENT_INTRO =
      '_SHOULD_SHOW_TARGETED_EVENT_INTRO';
  static const String _VERSION_APP = '_VERSION_APP';
  static const String _SHOW_DIALOG_PRESENT = 'SHOW_DIALOG_PRESENT';
  static const String _SHOW_TUTORIAL_JVC_DIALOG = 'SHOW_TUTORIAL_JVC_DIALOG';
  static const String _PINCODE = '_PINCODE';

  late SharedPreferences _prefs;

  Future<void> initialise() async {
    _prefs = await SharedPreferences.getInstance();
  }

  set fcmToken(String? token) {
    _prefs.setString(_FCM_TOKEN, token ?? '');
  }

  String? get fcmToken {
    if (!_prefs.containsKey(_FCM_TOKEN)) return null;
    return _prefs.getString(_FCM_TOKEN);
  }

   set pincode(String? pin) {
    _prefs.setString(_PINCODE, pin ?? '');
  }

  String? get pincode {
    if (!_prefs.containsKey(_PINCODE)) return null;
    return _prefs.getString(_PINCODE);
  }

  bool? get getShowDialogTutorial => _prefs.getBool(_SHOW_DIALOG_TUTORIAL);

  Future<void> setShowDialogTutorial(bool value) async {
    await _prefs.setBool(_SHOW_DIALOG_TUTORIAL, value);
  }

  bool get shouldShowTargetedEventIntro =>
      _prefs.getBool(_SHOULD_SHOW_TARGETED_EVENT_INTRO) ?? true;

  Future<void> setShouldShowTargetedEventIntro(bool value) async {
    await _prefs.setBool(_SHOULD_SHOW_TARGETED_EVENT_INTRO, value);
  }

  set saveWelcomeFinished(bool val) {
    _prefs.setBool(_WELCOME, val);
  }

  bool get isWelcomeFinished {
    return _prefs.getBool(_WELCOME) ?? false;
  }

  set isNotShowDialogPresent(bool? isNotShowDialogPresent) {
    _prefs.setBool(_SHOW_DIALOG_PRESENT, isNotShowDialogPresent ?? false);
  }

  bool? get isNotShowDialogPresent {
    if (!_prefs.containsKey(_SHOW_DIALOG_PRESENT)) return false;
    return _prefs.getBool(_SHOW_DIALOG_PRESENT);
  }

  bool get hasProfile {
    return _prefs.getString(_PROFILE) != null;
  }

  AccountModel? get getProfile {
    return hasProfile
        ? AccountModel.fromJson(json.decode(_prefs.getString(_PROFILE)!))
        : null;
  }

  set saveProfile(AccountModel attr) {
    _prefs.setString(_PROFILE, json.encode(attr.toJson()));
  }

  Future<void> clearUser() async {
    await _prefs.remove(_PROFILE);
  }

  String? get versionApp {
    return _prefs.getString(_VERSION_APP);
  }

  set versionApp(String? version) {
    if (version != null) {
      _prefs.setString(_VERSION_APP, version);
    }
  }

    set baseURLServer(String? baseUrl) {
    _prefs.setString(_BASE_URL, baseUrl ?? '');
  }

  String? get baseURLServer {
    if (!_prefs.containsKey(_BASE_URL)) return "";
    return _prefs.getString(_BASE_URL);
  }

   set keyURLServer(String? key) {
    _prefs.setString(_KEY_URL, key ?? '');
  }

  String? get keyURLServer {
    if (!_prefs.containsKey(_KEY_URL)) return "";
    return _prefs.getString(_KEY_URL);
  }

  set isTutorialJvcShown(bool? isNotShowDialogPresent) {
    _prefs.setBool(_SHOW_TUTORIAL_JVC_DIALOG, isNotShowDialogPresent ?? false);
  }

  bool? get isTutorialJvcShown {
    if (!_prefs.containsKey(_SHOW_TUTORIAL_JVC_DIALOG)) return false;
    return _prefs.getBool(_SHOW_TUTORIAL_JVC_DIALOG);
  }
}
