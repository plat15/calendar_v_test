import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../component/loading_popup.dart';
import '../component/message_popup.dart';
import 'base_viewmodel.dart';

class BaseBuilder<T extends BaseViewModel> extends StatelessWidget {
  const BaseBuilder({
    Key? key,
    required this.model,
    this.child,
    this.initialise,
    this.builder,
  }) : super(key: key);

  final TransitionBuilder? builder;
  final T model;
  final void Function(BuildContext, T)? initialise;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
        create: (ctx) {
          model.onLoadingDialog = () {
            print("onLoadingDialog");
            LoadingPopup.show(ctx);
          };

          model.onHideLoadingDialog = () {
            print("onHideLoadingDialog");
            Navigator.pop(context);
          };

          model.onMessage = (message) {
            MessageDialog.show(ctx, message);
          };

          model.onError = (message) {
            MessageDialog.show(ctx, message);
          };

          initialise?.call(ctx, model);

          return model;
        },
        builder: builder,
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: child,
        ));
  }

  Future<T?> pushPage<T>(BuildContext context, Widget page) {
    return Navigator.of(context)
        .push<T>(MaterialPageRoute(builder: (context) => page));
  }
}
