import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

import '../remote/local/shared_prefs.dart';
import '../remote/service/api_client.dart';
import 'di/locator.dart';

abstract class BaseViewModel extends ChangeNotifier {
  final prefs = locator<SharedPrefs>();
  final api = locator<ApiClient>();
  VoidCallback? onLoadingDialog, onHideLoadingDialog;
  ValueChanged<String>? onError, onMessage;

  void showLoading() {
    onLoadingDialog?.call();
  }

  void hideLoading() {
    onHideLoadingDialog?.call();
  }

  void showError(String message) {
    onError?.call(message);
  }

  FutureOr showCommonError() async {
    final connectivityResult = await Connectivity().checkConnectivity();

    if (connectivityResult == ConnectivityResult.none) {
      onError?.call("message_error_lost_internet_connection");
      return;
    }

    onError?.call("message_error_process_failed");
  }

  void showMessage(String message) {
    onMessage?.call(message);
  }
}
