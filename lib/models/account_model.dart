import 'package:json_annotation/json_annotation.dart';

part 'account_model.g.dart';

@JsonSerializable()
class AccountModel {
  @JsonKey(name: 'Username')
  String? username;

  @JsonKey(name: 'Ma_cty')
  String? maCty;

  @JsonKey(name: 'ten_cty')
  String? tenCty;

  @JsonKey(name: 'ma_cty')
  String? ma_cty;

  @JsonKey(name: 'ma_xe')
  String? maXe;

  @JsonKey(name: 'ten_xe')
  String? tenXe;

  @JsonKey(name: 'sdt')
  String? sdt;

  AccountModel();

  factory AccountModel.fromJson(Map<String, dynamic> json) =>
      _$AccountModelFromJson(json);

  Map<String, dynamic> toJson() => _$AccountModelToJson(this);
}
