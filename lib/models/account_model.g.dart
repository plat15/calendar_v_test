// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountModel _$AccountModelFromJson(Map<String, dynamic> json) => AccountModel()
  ..username = json['Username'] as String?
  ..maCty = json['Ma_cty'] as String?
  ..tenCty = json['ten_cty'] as String?
  ..ma_cty = json['ma_cty'] as String?
  ..maXe = json['ma_xe'] as String?
  ..tenXe = json['ten_xe'] as String?
  ..sdt = json['sdt'] as String?;

Map<String, dynamic> _$AccountModelToJson(AccountModel instance) =>
    <String, dynamic>{
      'Username': instance.username,
      'Ma_cty': instance.maCty,
      'ten_cty': instance.tenCty,
      'ma_cty': instance.ma_cty,
      'ma_xe': instance.maXe,
      'ten_xe': instance.tenXe,
      'sdt': instance.sdt,
    };
